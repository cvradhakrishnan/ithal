GIT=~/proj/ithal

SHELL:=/bin/bash

repo:=git@gitlab.com:cvradhakrishnan/ithal.git

pull:
	git pull --rebase --autostash origin master

push: pull
	git push origin master

commit: pull
	$(eval changed := $(shell git status \
		--porcelain | awk 'match($$1, "(A|D|M)"){print $$2}')) \
		git commit $(changed)

### ithal targes ###

# target to use LuaTeX
lua:
	lualatex $(f) ;


# target to use XeTeX
xe:
	open $(f).pdf

mview:
	open $(f).pdf

gitlog:
	git --no-pager log  --date=iso \
	  --pretty="format:\\gititem \\gitauthor{%an} %n \
	  \\gitdate{%ad}%n \\gittitle{%B} %n \\githash{%h}\\endgititem %n%n" \
	  > gitlog.tex

xclean: 
	@rm *~ *.xdv *.css *.aux *.xref *.4ct *.4tc *.idv *.lg *.log 

